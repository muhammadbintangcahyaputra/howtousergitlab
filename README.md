# Belajar Git Engine

## Untuk Download Git Repo
<pre>
git clone https://gitlab.com/muhammadbintangcahyaputra/howtousergitlab.git
</pre>

## Untuk Membersihkan Terminal
<pre>
clear
</pre>

## Untuk Masuk Ke Folder Repo
<pre>
cd howtousergitlab
</pre>

## Untuk Melihat Status Perubahan
<pre>
git status
</pre>

## Untuk Melabeli Dan Mengupload Perubahan
<pre>
git add .
git commit -m 'Perubahan Kode V1'
git push
</pre>

## Untuk Membuat Cabang Baru
<pre>
git checkout -b version1
</pre>

## Untuk Melabeli Dan Mengupload Perubahan
<pre>
git add .
git commit -m 'Add New File To New Branch'
git push --set-upstream origin version1
</pre>

## Untuk Melihat Existing Branch Dan Berpindah Branch
<pre>
git branch
git checkout main
git checkout version1
</pre>
